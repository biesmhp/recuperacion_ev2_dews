<?php
  require_once "ej4_productos.php";

  // Alimentación
  $ali1 = new Alimentacion(15,3.15,"Manzana",6,2021);
  $ali2 = new Alimentacion(8,5.40,"Patata",8,2021);

  // Electronica
  $ele1 = new Electronica(41,1450,"Slimbook Ventus",2);
  $ele2 = new Electronica(43,1600,"Oryx System76",2);

  // Array de productos
  $productos = [$ali1,$ali2,$ele1,$ele2];

  // Mostrar todos los productos
  foreach ($productos as $producto) {
    echo $producto;echo "</br>";
  }

  echo "</br>";echo "</br>";

  // Mostrar la clase con productos más caros
  $sumAlimentacion = 0;
  $sumElectronica = 0;
  foreach ($productos as $producto) {
    if ($producto instanceof Alimentacion) {
      $sumAlimentacion += $producto->precio;
    }elseif ($producto instanceof Electronica) {
      $sumElectronica += $producto->precio;
    }
  }

  if ($sumAlimentacion>$sumElectronica) {
    echo "En alimentacion se ha gastado más, con un total de ".$sumAlimentacion."€";
  }else{
    echo "En electrónica se ha gastado más, con un total de ".$sumElectronica."€";
  }
?>
