<?php
  class Empleado {
    private $sueldo;

    public function __construct($sueldo){
      $this->sueldo = $sueldo;
    }

    public function getSueldo(){
      return $this->sueldo;
    }
  }

  class Encargado extends Empleado {
    public function __construct($sueldo){
      parent::__construct($sueldo * 1.15);
      #$this->sueldo+$this->sueldo*0.15;

    }
  }

  $empleado1 = new Empleado(50);
  echo $empleado1->getSueldo();
  echo "</br>";

  $encargado1 = new Encargado(50);
  echo $encargado1->getSueldo();
  echo "</br>";
?>
