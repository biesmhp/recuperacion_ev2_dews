<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="{{url('/')}}">Zoologico</a>
    <button
      class="navbar-toggler"
      type="button"
      data-mdb-toggle="collapse"
      data-mdb-target="#navbarColor02"
      aria-controls="navbarColor02"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarColor02">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item active">
          <a class="nav-link" href="{{route('animales.index')}}">Listado de animales</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{route('animales.create')}}">Nuevo animal</a>
        </li>
      </ul>
      @if(Auth::check() )

      @else
      <ul class="navbar-nav">
        <!-- Badge -->
        <li class="nav-item">
          <a class="nav-link" href="{{url('login')}}">
            Login
            <span><img src="{{ url('/assets/img/person-icon.png') }}" height="32" width="32"></span>
          </a>
        </li>
      </ul>
      @endif
    </div>
  </div>
</nav>
