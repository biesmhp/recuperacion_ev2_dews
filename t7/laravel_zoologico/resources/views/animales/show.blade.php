@extends('layouts.master')
@section('title')
    Zoológico
@endsection
@section('contenido')
    <div class="row">
      <div class="col-sm-3">
        <img src="{{asset('assets/img')}}/{{$animal->imagen}}" alt="Imagen de {{$animal->especie}}" style="height:200px"/>
      </div>
      <div class="col-sm-9">
        {{--TODO: Datos delanimal--}}
        <h1>{{$animal->especie}} ({{$animal->getEdad()}} años)</h1>
        <h3>Peso:</h3>
        {{$animal->peso}}
        <br><br>
        <h3>Altura:</h3>
        {{$animal->altura}}
        <br><br>
        <h3>Descripción:</h3>
        {{$animal->descripcion}}
        <br>
      </div>
      <div class="col-sm-12">
        <a href="{{ route('animales.edit' , $animal) }}">
          <h4 style="min-height:45px;margin:5px 0 10px 0">
            Editar {{$animal->especie}}
          </h4>
        </a>
      </div>
    </div>
@endsection
