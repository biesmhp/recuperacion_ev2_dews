<?php
  // Clase
  class Coche {
    private $matricula;
    private $velocidad;

    public function __construct($matricula, $velocidad){
      $this->matricula = $matricula;
      $this->velocidad = $velocidad;
    }

    public function acelera($acelera){
      $velocidadN = $this->velocidad+=$acelera;
      if ($velocidadN>120) {
        $velocidadN = 120;
      }
      return $this->velocidad=$velocidadN;
    }

    public function frena($frena){
      $velocidadN = $this->velocidad-=$frena;
      if ($velocidadN<0) {
        $velocidadN = 0;
      }
      return $this->velocidad=$velocidadN;
    }

    public function muestra(){
      return "Matricula: ".$this->matricula." a ".$this->velocidad."km/h";
    }
  }

  // Pruebas
  $coche1 = new Coche("123h", 50);
  echo $coche1->muestra();
  echo "</br>";

  $coche1->acelera(90);
  echo $coche1->muestra();
  echo "</br>";

  $coche1->frena(200);
  echo $coche1->muestra();
  echo "</br>";

?>
