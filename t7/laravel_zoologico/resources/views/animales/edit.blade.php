@extends('layouts.master')
@section('title')
    Zoológico
@endsection
@section('contenido')
<div class="row">
  <div class="offset-md-3 col-md-6">
    <br>
    <div class="card">
      <div class="card-header text-center">Añadir animal</div>
      <div class="card-body" style="padding:30px">
        {{--TODO: Abrir el formulario e indicar el método POST --}}
        <form action="{{ route('animales.update', $animal->id) }}" method="post" enctype="multipart/form-data">
        {{--TODO: Protección contra CSRF --}}
        @csrf
        @method('put')
        <div class="form-group">
          <label for="especie">Especie</label>
          <input type="text" name="especie" id="especie" class="form-control" value="{{$animal->especie}}" required>
        </div>
          <div class="form-group">
            <label for="peso">Peso</label>
            <input type="number" name="peso" id="peso" min="0" step="1" value="{{$animal->peso}}" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="altura">Altura</label>
            <input type="number" name="altura" id="altura" min="0" step="0.2" value="{{$animal->altura}}" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="fechaNacimiento">Fecha de Nacimiento</label>
            <input type="date" name="fechaNacimiento" id="fechaNacimiento" value="{{$animal->fechaNacimiento}}" class="form-control" required>
          </div>
          <div class="form-group">
            <label for="alimentacion">Alimentación: </label>
            <br><label><em>Herbívoro</em></label><input type="radio" name="alimentacion" id="alimentacion" value="herbívoro" required>
            <br><label><em>Carnívoro</em></label><input type="radio" name="alimentacion" id="alimentacion" value="carnívoro" required>
            <br><label><em>Omnívoro</em></label><input type="radio" name="alimentacion" id="alimentacion" value="omnívoro" required>
          </div>
          <div class="form-group">
            <label for="descripcion">Descripción</label>
            <textarea name="descripcion" id="descripcion" class="form-control" rows="3">{{$animal->descripcion}}</textarea>
          </div>
            <div class="form-group">
              <label for="imagen">Imagen</label>
              <input type="file" name="imagen" id="imagen" class="form-control">
            </div>
            <div class="form-group text-center">
              <button type="submit" class="btn btn-success" style="padding:8px 100px;margin-top:25px;">Modificar animal</button>
            </div>
            {{--TODO: Cerrar formulario --}}
            </form>
          </div>
          </div>
        </div>
      </div>
@endsection
