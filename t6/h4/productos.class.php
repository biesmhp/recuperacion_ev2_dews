<?php
  require_once "supermercado.db.class.php";
  require_once "categoria.class.php";

  /**
   *
   */
  class Producto
  {
    protected $codigo, $precio, $nombre, $categoria;

    public function __construct($codigo, $precio, $nombre, $categoria)
    {
      $this->codigo = $codigo;
      $this->precio = $precio;
      $this->nombre = $nombre;
      $this->categoria = $categoria;
    }

    public function __toString()
    {
      return "::PARENT:: Codigo: ".$this->codigo.", Precio: ".$this->precio.", Nombre: ".$this->nombre.", Categoria: ".$this->categoria;
    }
  }

?>
