<?php
  require_once "supermercado.db.class.php";
  require_once "productos.class.php";
  require_once "alimentacion.class.php";
  require_once "electronica.class.php";

  // Estilo Viejo
  // // Objetos de clase Alimentacion()
  // $alimentacion1 = new Alimentacion("gh34",2.75,"Patatas Gourmet","tuberculos",2,2022);
  // $alimentacion2 = new Alimentacion("gj75",3,"Manzanas Golden","fruta",11,2021);
  //
  // // Objetos de clase Electronica()
  // $electronica1 = new Electronica("hy89", 1350, "Portatil Oryx", "portatiles", 2);
  // $electronica2 = new Electronica("kl18", 120, "Gloriuos Keyboard TKL", "teclado", 3);
  //
  // echo $alimentacion1;
  // echo "</br>";
  // echo $electronica1;

  // Estilo nuevo
  $productos = Database::getInstance()->getProductos();
?>
