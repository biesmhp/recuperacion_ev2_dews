<?php
  function print_r2($val){
    echo "<pre>";
    print_r($val);
    echo  "</pre>";
  }
  /**
   *
   */
  abstract class Medico
  {
    protected $nombre, $edad;
    protected $turno;

    public function __construct($nombre, $edad, $turno)
    {
      $this->nombre = $nombre;
      $this->edad = $edad;
      $this->turno = $turno;
    }

    public function __get($atributo)
    {
      return $this->$atributo;
    }

    public function mostrar()
    {
      return "\nNombre: ".$this->nombre.", Edad: ".$this->edad.", Turno: ".$this->turno;
    }
  }

  /**
   *
   */
  class Familia extends Medico
  {
    protected $num_pacientes;

    public function __construct($nombre, $edad, $turno, $num_pacientes)
    {
      parent::__construct($nombre, $edad, $turno);
      $this->num_pacientes = $num_pacientes;
    }

    public function __get($atributo)
    {
      return $this->$atributo;
    }

    public function mostrar()
    {
      return "Del padre: ".parent::mostrar().".\n Y del hijo: Numero de Pacientes: ".$this->num_pacientes;
    }
  }

  /**
   *
   */
  class Urgencia extends Medico
  {
    private $unidad;

    public function __construct($nombre, $edad, $turno, $unidad)
    {
      parent::__construct($nombre, $edad, $turno);
      $this->unidad = $unidad;
    }

    public function mostrar()
    {
      return "Del padre: ".parent::mostrar().".\n Y del hijo; Unidad: ".$this->unidad;
    }
  }

?>
