<?php
  require_once "supermercado.db.class.php";
  require_once "productos.class.php";
  require_once "categoria.class.php";

  /**
   *
   */
  class Alimentacion extends Producto
  {
    private $mesCaducidad, $añosCaducidad;

    public function __construct($codigo, $precio, $nombre, $categoria,$mesCaducidad,$añosCaducidad)
    {
      parent::__construct($codigo, $precio, $nombre, $categoria);
      $this->mesCaducidad = $mesCaducidad;
      $this->añosCaducidad = $añosCaducidad;
    }

    public function __toString()
    {
      return parent::__toString().". ::Hijo:: Caducidad; ".$this->mesCaducidad."/".$this->añosCaducidad;
    }
  }

?>
