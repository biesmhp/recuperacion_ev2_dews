<?php
  require_once "ej3_medico.php";

  // Objetos
  $familia1 = new Familia("Javi",35,"mañana",8);
  $familia2 = new Familia("Luis",72,"tarde",3);

  $urgencia1 = new Urgencia("Pedro",51,"tarde","dermatologia");
  $urgencia2 = new Urgencia("Rodolfo",67,"mañana","pediatria");

  // Array de los objetos
  $medicos = [$familia1,$familia2,$urgencia1,$urgencia2];

  #echo $familia1->mostrar();

  // Mostrar todos los médicos
  echo "Todos los médicos";echo "</br>";
  foreach ($medicos as $medico) {
    #print_r2($medico);echo "</br>";
    echo $medico->mostrar();echo "</br>";
    #var_dump($medico);
  }

  echo "</br>";echo "</br>";

  // Mostrar los médicos de tarde de urgencias de más de 60 años
  echo "Médicos de urgencia de más de 60 años";echo "</br>";
  foreach ($medicos as $medico) {
    if ($medico->edad>60&&$medico instanceof Urgencia) {
      echo $medico->mostrar();echo "</br>";
    }
  }

  echo "</br>";echo "</br>";

  // Al mandar el formulario
  if (isset($_POST["pacientes"])) {
    $nPacientes = $_POST["pacientes"];

    // Mostrar los médicos con más de X pacientes
    foreach ($medicos as $medico) {
      if ($medico instanceof Familia && $medico->num_pacientes>$nPacientes) {
        $arrayMedicos[] = $medico->mostrar();echo "</br>";
      }
    }
  }

?>

<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <hr>
    <!-- Formulario -->
    <form action="" method="post">
      <fieldset>
        <legend>Médicos por número de pacientes</legend>
        <label for="pacientes">Médicos con más de:</label>
        <input type="number" name="pacientes" min="0" placeholder="nº pacientes">
        <br>
        <input type="submit" name="enviar" value="Mostrar">
      </fieldset>
    </form>

    <!-- Respuesta del formulario -->
    <?php if (isset($arrayMedicos)): ?>
      <hr>
      <?php echo "Médicos con más de ".$nPacientes." pacientes:</br>" ?>
      <?php foreach ($arrayMedicos as $medicoA): ?>
        <?php echo $medicoA."</br>" ?>
      <?php endforeach; ?>
    <?php endif; ?>
  </body>
</html>
