<?php
  require_once "supermercado.db.class.php";
  require_once "productos.class.php";
  require_once "categoria.class.php";

  /**
   *
   */
  class Electronica extends Producto
  {
    private $plazoGarantia;

    public function __construct($codigo, $precio, $nombre, $categoria,$plazoGarantia)
    {
      parent::__construct($codigo, $precio, $nombre, $categoria);
      $this->plazoGarantia = $plazoGarantia;
    }

    public function __toString()
    {
      return parent::__toString().". ::Hijo:: Plazo de garantía: ".$this->plazoGarantia;
    }
  }

?>
