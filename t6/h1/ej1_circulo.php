<?php
  // Clase circulo
  Class Circulo {
    private $radio;

    // Constructor
    public function __construct($valorRadio) {
      $this->radio = $valorRadio;
    }

    // Getter y Setter
    public function getRadio() {
      return $this->radio;
    }

    public function setRadio($valorRadio) {
      $this->radio = $valorRadio;
    }

    // Métodos mágicos
    public function __get($atributo) {
      return $this->$atributo;
    }

    public function __set($atributo, $valor) {
      $this->$atributo = $valor;
    }

  }

  $a = new Circulo(5);
  #print_r($a);

  // Parte con Getter y Setter
  echo "El radio es: ".$a->getRadio();
  echo "</br>";
  $a->setRadio(14);
  echo "El radio es: ".$a->getRadio();
  echo "</br>";echo "</br>";

  // Parte con Mágicos
  echo "El radio es: ".$a->radio;
  echo "</br>";
  $a->radio = 9;
  echo "El radio es: ".$a->radio;
  echo "</br>";

?>
