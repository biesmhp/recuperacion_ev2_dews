<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AnimalController;
use App\Http\Controllers\InicioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [InicioController::class,'inicio'])->name('');

Route::get('animales', [AnimalController::class, 'index'])->name('animales.index');

Route::get('animales/crear', [AnimalController::class, 'create'])->name('animales.create');

Route::get('animales/restGET', [AnimalController::class,'rest'])->name('animales.rest'); //aqui

Route::get('animales/ajax', [AnimalController::class,'ajax'])->name('animales.ajax'); //aqui

Route::get('animales/{animal}', [AnimalController::class, 'show'])->name('animales.show');

Route::get('animales/{animal}/editar', [AnimalController::class, 'edit'])->name('animales.edit');

Route::post('animales', [AnimalController::class,'store'])->name('animales.store');

Route::put('animales/{animal}', [AnimalController::class,'update'])->name('animales.update');
