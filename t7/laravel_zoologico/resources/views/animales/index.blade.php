@extends('layouts.master')
@section('title')
    Zoológico
@endsection
@section('contenido')
    <h1>Listado de animales</h1>
    <div class="row">
      @foreach( $arrayAnimales as $animal)
      <div class="col-xs-12col-sm-6col-md-4">
        <a href="{{ route('animales.show' , $animal) }}">
          <img src="{{asset('assets/img')}}/{{$animal->imagen}}" alt="Imagen de {{$animal->especie}}" style="height:200px"/>
          <h4 style="min-height:45px;margin:5px 0 10px 0">
            {{$animal->especie}}
          </h4>
        </a>
      </div>
      @endforeach
    </div>
@endsection
