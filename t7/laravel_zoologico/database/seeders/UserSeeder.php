<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(5)->create();

        $a = new User();
        $a->name = 'pedro';
        $a->email = 'pedro.rf@gmail.com';
        $a->password = password_hash("toor", PASSWORD_BCRYPT);
        $a->save();

      $this->command->info('Tabla usuarios inicializada con datos');
    }
}
