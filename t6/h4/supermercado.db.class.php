<?php
  require_once "categoria.class.php";
  require_once "productos.class.php";
  require_once "alimentacion.class.php";
  require_once "electronica.class.php";

  /**
   *
   */
  class Database
  {
    // Instancia
    private static $instance = null;
    public static function getInstance(): Database
    {
        if (self::$instance === null) {
            self::$instance = new Database();
        }
        return self::$instance;
    }

    // MySQLi
    // Conexxión a base de datos
    private function conexionMy(){
      $conexion = new mysqli('localhost','root','','supermercado');
      $error = $conexion->connect_errno;
      if($error!=null){
        exit();
      }else{
        return $conexion;
      }
    }

    // PDO
    // Conexxión a base de datos
    private function conexionPDO(){
      $opciones= array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
      $conexion= new PDO('mysql:host=localhost;dbname=supermercado', 'root', '' , $opciones);
      return $conexion;
    }

    ################################## Otras funciones #########################################
    public function getProductos()
    {
      $conexion = conexionPDO();
      $sql = "SELECT * FROM productos";
      $preparada = $conexion->prepare($sql);
      $preparada->execute();
      var_dump($preparada);
    }
  }

?>
