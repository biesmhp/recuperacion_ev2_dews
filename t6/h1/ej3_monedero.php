<?php
  class Monedero {
    private $dinero;
    private static $numero_monederos = 0;

    // Constructor
    public function __construct($dinero){
      $this->dinero = $dinero;
      self::$numero_monederos++;
    }

    // Métodos
    public function meterDinero($dinero){
      return $this->dinero += $dinero;
    }

    public function sacarDinero($dinero){
      if ($this->dinero-$dinero>=0) {
        return $this->dinero -= $dinero;
      }else{
        echo "No puedes sacar más dinero del que hay en la cuenta";echo "</br>";
      }
    }

    public function mostrarDinero(){
      return $this->dinero;
    }

    // Método para estáticos
    public static function nuevoMonedero(){
      self::$numero_monederos++;
    }

    public function __destruct(){
      self::$numero_monederos--;
    }
  }

  $monedero1 = new Monedero(50);

  echo $monedero1->mostrarDinero();
  echo "</br>";

  $monedero1->meterDinero(40);
  echo $monedero1->mostrarDinero();
  echo "</br>";

  $monedero1->sacarDinero(100);
  echo $monedero1->mostrarDinero();
  echo "</br>";
?>
