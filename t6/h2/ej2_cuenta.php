<?php
  /**
   *
   */
  class Cuenta
  {
    // Los atributos de una clase padre deberán ser "protected" para que las clases hijas tengan acceso
    protected $numero, $titular, $saldo;

    function __construct($numero, $titular, $saldo)
    {
      $this->numero = $numero;
      $this->titular = $titular;
      $this->saldo = $saldo;
    }

    public function ingreso($cantidad){
      $this->saldo += $cantidad;
    }

    public function reintegro($cantidad){
      if ($this->saldo-$cantidad>=0) {
        $this->saldo -= $cantidad;
      }else{
        return false;
      }
    }

    public function esPreferencial($cantidad){
      return $this->saldo>$cantidad ? true : false;
    }

    public function mostrar(){
      return "\n Saldo: ".$this->saldo."\n Titular: ".$this->titular."\n Numero de cuenta: ".$this->numero;
    }
  }


  /**
   *
   */
  class CuentaCorriente extends Cuenta
  {
    private $cuota_mantenimiento;

    function __construct($numero, $titular, $saldo, $cuota_mantenimiento)
    {
      parent::__construct($numero, $titular, $saldo-$cuota_mantenimiento);
      $this->cuota_mantenimiento = $cuota_mantenimiento;
    }

    public function reintegro($cantidad){
      if ($this->saldo<20) {
        if ($this->saldo-$cantidad>=0) {
          $this->saldo -= $cantidad;
        }
      }else{
        return false;
      }
    }

    public function mostrar(){
      return parent::mostrar()."\n Cuota: ".$this->cuota_mantenimiento;
    }
  }

  /**
   *
   */
  class CuentaAhorro extends Cuenta
  {
    private $comision_apertura, $intereses;

    function __construct($numero, $titular, $saldo, $comision_apertura, $intereses)
    {
      parent::__construct($numero, $titular, $saldo-$comision_apertura);
      $this->intereses = $intereses;
    }

    public function aplicaInteres(){
      $this->saldo += $this->saldo * $this->intereses;
    }

    public function mostrar(){
      return parent::mostrar()."\n Interes: ".$this->intereses;
    }
  }


?>
