<?php
  require_once 'ej2_cuenta.php';

  // Clase Cuenta
  $cuenta = new Cuenta("N680", "Javi", 538);
  echo $cuenta->mostrar();
  echo "</br>";

  // Clase CuentaCorriente
  $cuentaCorriente = new CuentaCorriente("N680", "Javi", 538, 38);
  echo $cuentaCorriente->mostrar();
  echo "</br>";

  // Clase CuentaAhorro
  $cuentaAhorro = new CuentaAhorro("N680", "Javi", 538, 500, 0.1);
  echo $cuentaAhorro->mostrar();
  echo "</br>";
  $cuentaAhorro->aplicaInteres();
  echo $cuentaAhorro->mostrar();
  echo "</br>";

?>
