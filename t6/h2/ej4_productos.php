<?php
  function print_r2($val){
    echo "<pre>";
    print_r($val);
    echo  "</pre>";
  }

  /**
   *
   */
  abstract class Productos
  {
    protected $codigo, $precio, $nombre;

    public function __construct($codigo, $precio, $nombre)
    {
      $this->codigo = $codigo;
      $this->precio = $precio;
      $this->nombre = $nombre;
    }

    public function __get($atributo)
    {
      return $this->$atributo;
    }

    public function __set($variable, $valor)
    {
      $this->$atributo = $valor;
    }

    public function __toString()
    {
      return "::Padre:: Codigo: ".$this->codigo.", Precio: ".$this->precio.", Nombre: ".$this->nombre;
    }
  }

  /**
   *
   */
  class Alimentacion extends Productos
  {
    private $mes,$año;

    public function __construct($codigo, $precio, $nombre, $mes, $año)
    {
      parent::__construct($codigo, $precio, $nombre);
      $this->mes = $mes;
      $this->año = $año;
    }

    public function __get($atributo)
    {
      return $this->$atributo;
    }

    public function __set($variable, $valor)
    {
      $this->$atributo = $valor;
    }

    public function __toString()
    {
      return parent::__toString().". ::Hijo:: Mes: ".$this->mes." y año: ".$this->año." de caducidad.";
    }
  }

  /**
   *
   */
  class Electronica extends Productos
  {
    private $garantia;

    public function __construct($codigo, $precio, $nombre, $garantia)
    {
      parent::__construct($codigo, $precio, $nombre);
      $this->garantia = $garantia;
    }

    public function __get($atributo)
    {
      return $this->$atributo;
    }

    public function __set($variable, $valor)
    {
      $this->$atributo = $valor;
    }

    public function __toString()
    {
      return parent::__toString().". ::Hijo:: Garantia: ".$this->garantia." años.";
    }
  }


?>
